<?php

/**
 * @file
 * Plugin for controlling access based on the path arguments rule.
 */

$plugin = array(
  'title' => t('Valid Path Argument'),
  'description' => t('Verify the argument in the path.'),
  'callback' => 'ctools_valid_path_arguments_access_check',
  'settings form' => 'ctools_valid_path_arguments_access_settings',
  'summary' => 'ctools_valid_path_arguments_access_summary',
  'defaults' => array('arguments' => ''),
);

/**
 * Settings form.
 */
function ctools_valid_path_arguments_access_settings($form, &$form_state, $config) {
  $form['settings']['arguments'] = array(
    '#title' => t('Valid path arguments'),
    '#description' => t('Enter the valid path arguments. Seperated by comma. Leave blank to refuse any argument in the path,'),
    '#type' => 'textarea',
    '#required' => FALSE,
    '#default_value' => $config['arguments']
  );

  return $form;
}

/**
 * Check for access.
 */
function ctools_valid_path_arguments_access_check($config, $context) {
	$menu_item = menu_get_item();

	$href = $menu_item['href'];     // node/1
	$current_path = current_path(); // node/1/broken
	
	// Check if the actual path ($current_path) is different than the Drupal assumption ($href)
	if ($href !== $current_path) {
		$arguments = explode(',', $config['arguments']);
		foreach ($arguments as $argument) {
		  if ($current_path === $href . '/' . $argument) {
		  	return TRUE;
		  }
		}
		// Return false would redirect user to 404 page which will hanlde the HTTP response as well
		return FALSE;
	}
	
	return TRUE;
}

/**
 * Provide a summary description.
 */
function ctools_valid_path_arguments_access_summary($config, $context) {
  if (empty($config['arguments'])) {
    return t('Refuse any argument');
  }
  
  return t('Valid argument: @arguments', array('@arguments' => $config['arguments']));
}
